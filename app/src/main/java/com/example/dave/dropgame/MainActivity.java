package com.example.dave.dropgame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * The MainActivity for a game of DropGame: This game tasks the player with
 * the responsibility of guiding a poorly drawn stick figure down an endless
 * pit without hitting any walls on the way down. As the stick figure falls
 * deeper into the pit, the figure falls more and more quickly, and the
 * passage the figure must navigate becomes smaller and smaller, until
 * the stick figure finally crashes in an explosion of gore.
 *
 * The MainActivity serves as the Controller in the MVC pattern. Its primary
 * responsibility is to hold a DropGameThread object, which runs the game in
 * its own thread. It also assigns OnClickListeners to button objects that
 * the user can press; these OnClickListeners can interact with the
 * DropGameModel that the DropGameThread is running, and do things like move
 * the player's avatar or reset the game.
 */
public class MainActivity extends AppCompatActivity {
    /**
     * Used to print to the log
     */
    public static final String TAG = "myTag";
    /**
     * Counts the number of threads: multiple threads break the game
     */
    public static int numThreads = 0;

    /**
     * A thread that runs the game loop
     */
    private class DropGameThread extends Thread {
        private long mTime;

        public DropGameThread() {
            mTime = System.currentTimeMillis();
            numThreads += 1;
            System.out.println("numThreads=" + numThreads);
        }

        @Override
        public void run() {
            while (!mModel.isGameOver()) {
                long currentTime = System.currentTimeMillis();
                Log.i(TAG, "timeDelta=" + (currentTime - mTime));
                mModel.update(currentTime - mTime);
                boolean drew = mView.doDraw(
                        mModel.getmPlayerPositionX(),
                        mModel.getmPlayerPositionY(),
                        mModel.getmHolePosition(),
                        mModel.getmHoleWidth(),
                        mModel.isGameOver()
                );
                if (drew) {
                    mTime = currentTime;
                }
            }
            numThreads -= 1;
        }
    }


    /**
     * The view, in MVC
     */
    private DropGameView mView;
    /**
     * The model, in MVC
     */
    private DropGameModel mModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get a reference to the DropGameView
        mView = (DropGameView) findViewById(R.id.game_view);

        /**
         * OnClickListener for the left button
         */
        Button btnLeft = (Button) findViewById(R.id.btnLeft);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mModel != null) {
                    mModel.moveLeft();
                }
            }
        });
        /**
         * OnClickListener for the right button
         */
        Button btnRight = (Button) findViewById(R.id.btnRight);
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mModel != null) {
                    mModel.moveRight();
                }
            }
        });
        /**
         * OnClickListener for the New Game button
         */
        Button btnNewGame = (Button) findViewById(R.id.btnNewGame);
        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Refuse to start a new game if there's already a thread
                // running; multiple threads interact with the same
                // DropGameModel object and cause problems
                if (numThreads > 0) {
                    Toast.makeText(
                            MainActivity.this,
                            "Can't play more than one game at once;\n" +
                                    "please try falling to your gory death first",
                            Toast.LENGTH_SHORT).show();
                } else {
                    mModel.setupNewBoard(true);
                    DropGameThread thread = new DropGameThread();
                    thread.start();
                }
            }
        });
    }

    /**
     * Method used to build a DropGameModel object. The DropGameView calls
     * this method once it has found the screen's and avatar's dimensions
     * @param screenWidth   Width of the SurfaceView
     * @param screenHeight  Height of the SurfaceView
     * @param playerWidth   Width of the player's avatar stick-figure
     * @param playerHeight  Height of the player's avatar stick-figure
     */
    public void makeGame(int screenWidth, int screenHeight,
                          int playerWidth, int playerHeight) {
        Log.i(TAG, "makeGame");

        if (screenWidth > 0 && screenHeight > 0) {
            mModel = new DropGameModel(screenWidth, screenHeight,
                    playerWidth, playerHeight);
            DropGameThread thread = new DropGameThread();
            thread.start();
        }
    }
}
