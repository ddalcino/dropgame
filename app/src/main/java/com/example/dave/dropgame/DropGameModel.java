package com.example.dave.dropgame;

import android.util.Log;

import java.util.Random;

/**
 * DropGameModel: A data model that holds the current state of a game, and
 * provides control logic to progress the state of the game. Fills the Model
 * portion of the MVC pattern.
 *
 * Created by dave on 11/16/15.
 */
public class DropGameModel {
    /**
     * Random number generator
     */
    public static Random rand = new Random();

    /**
     * The distance the player can move horizontally on each button press,
     * in pixels
     */
    public static final int H_VELOCITY = 25;
    /**
     * The player's initial vertical velocity, in pixels per second. A positive
     * value means the player is moving downward across the screen.
     */
    public static final float V_VELOCITY_INITIAL = 200.0f;
    /**
     * In each stage, the player's vertical velocity is multiplied by this amount.
     */
    public static final float V_VELOCITY_MULTIPLIER = 1.1f;

    /**
     * The thickness of the walls at the bottom of the screen, in pixels
     */
    public static final int WALL_THICKNESS = 20;

    /**
     * The initial size of the hole the player is trying to navigate through is
     * this number times half the width of the player. On each new level, the
     * hole gets smaller by 1 pixel.
     */
    public static final int HOLE_SIZE_MULTIPLIER_INIT = 3;

    /**
     * Integers that denote the size of the playing field, in pixels
     */
    private int mScreenWidth, mScreenHeight;

    /**
     * The dimensions of the player's avatar
     */
    public final int mPlayerWidth, mPlayerHeight;

    /**
     * Half the width of the player's avatar; stored for convenience
     */
    public final int mHalfPlayerWidth;

    /**
     * The vertical coordinate, in pixels, in which the player might first
     * collide with the wall
     */
    public final int mVerticalCollisionCutoff;

    /**
     * The width of the hole that the player's avatar will fall through
     */
    private int mHoleWidth;

    /**
     * The distance in pixels between the left side of the playing field and
     * the left side of the hole
     */
    private int mHolePosition;

    /**
     * The vertical and horizontal coordinates of the player's avatar;
     * specifically, the coordinates of the upper left-hand corner
     */
    private float mPlayerPositionX, mPlayerPositionY;
    /** The player's vertical velocity */
    private float mPlayerVerticalVelocity;

    /** Whether or not the player has collided with the wall */
    private boolean mHasCollided;

    /**
     * Constructor
     * @param screenWidth   Width of the SurfaceView
     * @param screenHeight  Height of the SurfaceView
     * @param playerWidth   Width of the player's avatar stick-figure
     * @param playerHeight  Height of the player's avatar stick-figure
     */
    public DropGameModel(int screenWidth, int screenHeight,
                         int playerWidth, int playerHeight) {
        mScreenWidth = screenWidth;
        mScreenHeight = screenHeight;
        mPlayerWidth = playerWidth;
        mPlayerHeight = playerHeight;
        mHalfPlayerWidth = playerWidth/2;
        mVerticalCollisionCutoff = mScreenHeight-WALL_THICKNESS-mPlayerHeight;

        setupNewBoard(true);
    }

    /**
     * Sets up a new board. If this is a new game, it resets the vertical speed
     * and hole size to their default starting values; otherwise it increases
     * the speed and decreases the hole size. It also sets the hole position to
     * a new random location.
     * @param isNewGame whether or not this is a new game
     */
    public void setupNewBoard(boolean isNewGame) {
        if(isNewGame) {
            mHoleWidth = HOLE_SIZE_MULTIPLIER_INIT * mHalfPlayerWidth;
            mPlayerVerticalVelocity = V_VELOCITY_INITIAL;
        } else {
            // make the hole smaller
            mHoleWidth -= 1;
            // ensure that it's still possible to navigate down the hole
            if (mHoleWidth < mPlayerWidth + H_VELOCITY) {
                mHoleWidth = mPlayerWidth + H_VELOCITY;
            }
            mPlayerVerticalVelocity *= V_VELOCITY_MULTIPLIER;
        }
        mHolePosition = (int) ((mScreenWidth-mHoleWidth) * rand.nextDouble());
        mPlayerPositionX = mScreenWidth/2 - mHalfPlayerWidth;
        mPlayerPositionY = 0.0f;
        mHasCollided = false;
    }

    /**
     * Updates the state of the game by timeDeltaMS milliseconds.
     *
     * @param timeDeltaMS   number of milliseconds since the last update
     * @return          true if the game is still going,
     *                  false if the player has collided with a wall
     */
    public boolean update(long timeDeltaMS) {
        // update vertical position
        mPlayerPositionY += mPlayerVerticalVelocity *
                ((float) timeDeltaMS / 1000.0f);
        // check for collisions: First check that the player has passed the
        // point where s/he may collide with a wall
        if (mPlayerPositionY >= mVerticalCollisionCutoff) {
            if (mPlayerPositionX < mHolePosition ||  // Left side of the hole
                    mPlayerPositionX > mHolePosition + mHoleWidth - mPlayerWidth) { // right side
                mHasCollided = true;
                Log.i(MainActivity.TAG, "v_velocity="+mPlayerVerticalVelocity);
            } else if (mPlayerPositionY >= mScreenHeight - WALL_THICKNESS) {
                setupNewBoard(false);
            }
        }
        return !mHasCollided;
    }

    /**
     * Moves the player left. Susceptible to race conditions!
     */
    public void moveLeft() {
        mPlayerPositionX -= H_VELOCITY;
        if (mPlayerPositionX < 0) mPlayerPositionX = 0;
    }
    /**
     * Moves the player right. Susceptible to race conditions!
     */
    public void moveRight() {
        mPlayerPositionX += H_VELOCITY;
        if (mPlayerPositionX > mScreenWidth - mPlayerWidth) {
            mPlayerPositionX = mScreenWidth - mPlayerWidth;
        }
    }

    // Getters
    public int getmPlayerPositionX() {
        return (int) mPlayerPositionX;
    }

    public int getmPlayerPositionY() {
        return (int) mPlayerPositionY;
    }

    public int getmHoleWidth() {
        return mHoleWidth;
    }

    public int getmHolePosition() {
        return mHolePosition;
    }

    public boolean isGameOver() {
        return mHasCollided;
    }
}