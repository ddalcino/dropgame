package com.example.dave.dropgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


/**
 * DropGameView: a SurfaceView object specifically designed to display a DropGame
 * Extends SurfaceView, in order to draw rapidly to the screen
 * Implements SurfaceHolder.Callback, in order to obtain the dimensions of the
 * playing field, in pixels
 *
 * Created by dave on 11/16/15.
 */
public class DropGameView extends SurfaceView implements SurfaceHolder.Callback {

    /**
     * A reference to the controller
     */
    MainActivity mController;

    /**
     * Integers that denote the size of the SurfaceView object, in pixels
     */
    private int mScreenWidth=-1, mScreenHeight=-1;
    /**
     * The dimensions of the player's avatar
     */
    public final int mPlayerWidth, mPlayerHeight;

    /**
     * A bitmap that holds the player's avatar: a poorly drawn stick figure.
     * The worse it's drawn, the more humorous the appearance.
     */
    private final Bitmap mBmpStickman;

    /**
     * Paint objects (like a paintbrush), defined for three colors
     */
    private Paint mBlackPaint, mWhitePaint, mRedPaint;

    /**
     * A SurfaceHolder object: used to obtain locks on the Canvas that you can
     * draw on
     */
    private SurfaceHolder mHolder;


    /**
     * Constructor for DropGameView
     * @param context   A reference to the Activity that contains this object
     * @param attributeSet  The set of attributes defined by the xml file that
     *                      defines this object
     */
    public DropGameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Log.i(MainActivity.TAG, "GameView constructor called");

        // remember the reference to the Controller, so we can access data in
        // the DropGameModel object
        mController = (MainActivity) context;

        // Open the stickman.png resource and turn it into a bitmap
        mBmpStickman = BitmapFactory.decodeResource(getResources(),
                R.drawable.stickman);
        // Set the dimensions of the player's avatar
        mPlayerHeight = mBmpStickman.getHeight();
        mPlayerWidth = mBmpStickman.getWidth();

        // register our interest in hearing about changes to our surface
        mHolder = getHolder();
        mHolder.addCallback(this);

        // Define paintbrushes:
        mBlackPaint = new Paint();
        mBlackPaint.setStyle(Paint.Style.FILL);
        mBlackPaint.setColor(Color.BLACK);

        mWhitePaint = new Paint();
        mWhitePaint.setStyle(Paint.Style.FILL);
        mWhitePaint.setColor(Color.WHITE);

        mRedPaint = new Paint();
        mRedPaint.setStyle(Paint.Style.FILL);
        mRedPaint.setColor(Color.RED);
    }

    /**
     * This is called immediately after the surface is first created.
     * Implementations of this should start up whatever rendering code
     * they desire.  Note that only one thread can ever draw into
     * a {@link Surface}, so you should not draw into the Surface here
     * if your normal rendering will be in another thread.
     *
     * @param holder The SurfaceHolder whose surface is being created.
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(MainActivity.TAG, "surfaceCreated called");
    }

    /**
     * This is called immediately after any structural changes (format or
     * size) have been made to the surface.  You should at this point update
     * the imagery in the surface.  This method is always called at least
     * once, after {@link #surfaceCreated}.
     *
     * @param holder The SurfaceHolder whose surface has changed.
     * @param format The new PixelFormat of the surface.
     * @param width  The new width of the surface.
     * @param height The new height of the surface.
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(MainActivity.TAG, "surfaceChanged called");
        Log.i(MainActivity.TAG, "int format=" + format + ", int width=" + width +
                ", int height="+height);
        // A format value of 4 denotes PixelFormat.RGB_565, according to docs
        mScreenHeight = height;
        mScreenWidth = width;

        // Tell the Controller to start a new game: Since the constructor
        // requires knowledge of the size of the screen, we cannot start a new
        // game until the surfaceChanged callback method is called.
        mController.makeGame(mScreenWidth, mScreenHeight,
                mPlayerWidth, mPlayerHeight);
    }

    /**
     * This is called immediately before a surface is being destroyed. After
     * returning from this call, you should no longer try to access this
     * surface.  If you have a rendering thread that directly accesses
     * the surface, you must ensure that thread is no longer touching the
     * Surface before returning from this function.
     *
     * @param holder The SurfaceHolder whose surface is being destroyed.
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(MainActivity.TAG, "surfaceDestroyed called");

    }

    /**
     * Obtains
     * Draws the background, the stickman, and the walls at the bottom of the
     * screen to the provided Canvas.
     * @param x The x coordinate of the player
     * @param y The y coordinate of the player
     * @param holePosition  The position of the leftmost edge hole: this will
     *                      change each time the board is reset
     * @param holeWidth     The width of the hole, in pixels: this will also
     *                      change each time the board is reset
     * @return  true if the lock on the canvas was drawn to successfully,
     *          otherwise false
     */
    public boolean doDraw(int x, int y, int holePosition, int holeWidth, boolean isGameOver) {
        Log.i(MainActivity.TAG, "doDraw()");

        // Obtain a lock on the Canvas so that no-one else can draw on it while
        // the DropGameView is drawing on it
        Canvas canvas = mHolder.lockCanvas();
        if (canvas != null) {

            // Clear the whole screen:
            // Make a rectangle that encompasses the whole SurfaceView:
            Rect r = new Rect(0, 0, mScreenWidth, mScreenHeight);

            // Draw the rectangle to the canvas: Use the white brush if the
            // game is still running, or the red brush if the player is dead:
            canvas.drawRect(r, (isGameOver ? mRedPaint : mWhitePaint));

            // Draw the hole into which the stickman can fall: This is drawn by
            // drawing black rectangles where the stickman will crash into if he
            // misses the hole.
            Rect wallLeft = new Rect(
                    0,
                    mScreenHeight - DropGameModel.WALL_THICKNESS,
                    holePosition,
                    mScreenHeight);

            Rect wallRight = new Rect(
                    holePosition + holeWidth,
                    mScreenHeight - DropGameModel.WALL_THICKNESS,
                    mScreenWidth,
                    mScreenHeight);

            // Draw the walls at the left and right side of the hole
            canvas.drawRect(wallLeft, mBlackPaint);
            canvas.drawRect(wallRight, mBlackPaint);

            // Construct a matrix that will translate the stickman into the
            // proper position
            Matrix translationMat = new Matrix();
            translationMat.setTranslate(x, y);
            Log.i(MainActivity.TAG, "translationMat=" + translationMat.toString());

            // Draw the stickman:
            canvas.drawBitmap(mBmpStickman, translationMat, null);

            // Release locks on the Canvas and allow it to display to the screen
            mHolder.unlockCanvasAndPost(canvas);
            return true;
        } else {
            return false;
        }
    }
}
